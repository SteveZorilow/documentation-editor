﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str">This project contains all files necessary for the &lt;b&gt;Project Explorer XControl &lt;/b&gt;and a &lt;b&gt;Test.vi &lt;/b&gt;that is an example of the functionality of the &lt;b&gt;Project Explorer XControl.xctl&lt;/b&gt;.&lt;b&gt; &lt;/b&gt; &lt;b&gt; &lt;/b&gt;©2013-2019, Q Software Innovations, LLC.

Written in National Instruments LabVIEW™ 2019.

Authored by Quentin Alldredge
Q@qsoftwareinnovations.com
www.qsoftwareinnovations.com
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Right-Click Actions" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Child Actions" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Add File.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Add File/Add File.lvclass"/>
				<Item Name="Add Folder Auto-Populating.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Add Folder Auto-Populating/Add Folder Auto-Populating.lvclass"/>
				<Item Name="Add Folder Snapshot.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Add Folder Snapshot/Add Folder Snapshot.lvclass"/>
				<Item Name="Add SubMenu.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Add SubMenu/Add SubMenu.lvclass"/>
				<Item Name="Collapse All.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Collapse All/Collapse All.lvclass"/>
				<Item Name="Expand All.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Expand All/Expand All.lvclass"/>
				<Item Name="Explore.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Explore/Explore.lvclass"/>
				<Item Name="New Control.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/New Control/New Control.lvclass"/>
				<Item Name="New Folder.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/New Folder RCA/New Folder.lvclass"/>
				<Item Name="New SubMenu.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/New RCA/New SubMenu.lvclass"/>
				<Item Name="New VI.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/New VI RCA/New VI.lvclass"/>
				<Item Name="New Virtual Folder.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/New Virtual Folder RCA/New Virtual Folder.lvclass"/>
				<Item Name="Open.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Open/Open.lvclass"/>
				<Item Name="Refresh.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Refresh RCA/Refresh.lvclass"/>
				<Item Name="Remove from Project.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Remove from Project/Remove from Project.lvclass"/>
				<Item Name="Save.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Save/Save.lvclass"/>
				<Item Name="Separator.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Separator/Separator.lvclass"/>
				<Item Name="Show on Block Diagram.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Show on Block Diagram/Show on Block Diagram.lvclass"/>
				<Item Name="Show on Front Panel.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Show on Front Panel/Show on Front Panel.lvclass"/>
				<Item Name="Stop Auto-Populating.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Actions/Stop Auto-Populating/Stop Auto-Populating.lvclass"/>
			</Item>
			<Item Name="Right-Click Action.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Actions/Right-Click Action.lvclass"/>
			<Item Name="Test RCA.vi" Type="VI" URL="../SubVIs/Right-Click Actions/Test RCA.vi"/>
		</Item>
		<Item Name="Right-Click Menus" Type="Folder">
			<Item Name="Child Menus" Type="Folder">
				<Item Name="Auto-Populating Folder RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Auto-Populating Folder RCM/Auto-Populating Folder RCM.lvclass"/>
				<Item Name="Document LVProject RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Document LVProject RCM/Document LVProject RCM.lvclass"/>
				<Item Name="Document RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Document RCM/Document RCM.lvclass"/>
				<Item Name="Library RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Library RCM/Library RCM.lvclass"/>
				<Item Name="LVClass RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/LVClass RCM/LVClass RCM.lvclass"/>
				<Item Name="My Computer RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/My Computer RCM/My Computer RCM.lvclass"/>
				<Item Name="New Library RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/New Library RCM/New Library RCM.lvclass"/>
				<Item Name="New LVClass RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/New LVClass RCM/New LVClass RCM.lvclass"/>
				<Item Name="New VI RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/New VI RCM/New VI RCM.lvclass"/>
				<Item Name="New XControl.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/New XControl RCM/New XControl.lvclass"/>
				<Item Name="Project RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Project RCM/Project RCM.lvclass"/>
				<Item Name="VI RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/VI RCM/VI RCM.lvclass"/>
				<Item Name="Virtual Folder RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/Virtual Folder RCM/Virtual Folder RCM.lvclass"/>
				<Item Name="XControl RCM.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Menus/XControl RCM/XControl RCM.lvclass"/>
			</Item>
			<Item Name="Right-Click Menu.lvclass" Type="LVClass" URL="../SubVIs/Right-Click Menus/Right-Click Menu.lvclass"/>
		</Item>
		<Item Name="Support Files" Type="Folder">
			<Item Name="License Agreement.txt" Type="Document" URL="../Support Files/License Agreement.txt"/>
			<Item Name="Project Explorer XControl Functions.xlsx" Type="Document" URL="../Support Files/Project Explorer XControl Functions.xlsx"/>
			<Item Name="ProjectExplorer8.png" Type="Document" URL="../Support Files/ProjectExplorer8.png"/>
			<Item Name="ProjectExplorer16.png" Type="Document" URL="../Support Files/ProjectExplorer16.png"/>
			<Item Name="ProjectExplorer24.png" Type="Document" URL="../Support Files/ProjectExplorer24.png"/>
			<Item Name="ProjectExplorer32.png" Type="Document" URL="../Support Files/ProjectExplorer32.png"/>
			<Item Name="ProjectExplorer128.pdn" Type="Document" URL="../Support Files/ProjectExplorer128.pdn"/>
			<Item Name="ProjectExplorer128.png" Type="Document" URL="../Support Files/ProjectExplorer128.png"/>
			<Item Name="ProjectExplorerScreenshot.png" Type="Document" URL="../Support Files/ProjectExplorerScreenshot.png"/>
		</Item>
		<Item Name="SupportVIs" Type="Folder">
			<Item Name="OVIWD Library Files" Type="Folder">
				<Item Name="Abilities" Type="Folder"/>
				<Item Name="SubVIs" Type="Folder"/>
				<Item Name="Open VI When Dropped.lvlib" Type="Library" URL="../SupportVIs/OVIWD Library Files/Open VI When Dropped.lvlib"/>
			</Item>
			<Item Name="Example VI.vi" Type="VI" URL="../SupportVIs/Example VI.vi"/>
			<Item Name="Open Example VI.vi" Type="VI" URL="../SupportVIs/Open Example VI.vi"/>
			<Item Name="Place Project Explorer XControl.vi" Type="VI" URL="../SupportVIs/Place Project Explorer XControl.vi"/>
		</Item>
		<Item Name="Project Explorer XControl.xctl" Type="XControl" URL="../Project Explorer XControl.xctl"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Draw Multiple Lines.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Multiple Lines.vi"/>
				<Item Name="Draw Text at Point.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text at Point.vi"/>
				<Item Name="Draw Text in Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text in Rect.vi"/>
				<Item Name="Empty Picture" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Empty Picture"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="PCT Pad String.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/PCT Pad String.vi"/>
				<Item Name="Picture to Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Picture to Pixmap.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Pen State.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Set Pen State.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="Get All Descendents.vi" Type="VI" URL="../../Common/Get All Descendents.vi"/>
			<Item Name="Pause UI Updates (Reference).vi" Type="VI" URL="../../Common/Pause UI Updates/Pause UI Updates (Reference).vi"/>
			<Item Name="Pause UI Updates.vi" Type="VI" URL="../../Common/Pause UI Updates.vi"/>
			<Item Name="Start UI Updates (Reference).vi" Type="VI" URL="../../Common/Start UI Updates/Start UI Updates (Reference).vi"/>
			<Item Name="Start UI Updates.vi" Type="VI" URL="../../Common/Start UI Updates.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="PEX for Documentation Editor" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{EFD06301-8B61-40FB-BFB1-2B1981FF4F43}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">PEX for Documentation Editor</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/DE_PEX</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{73B9BFF6-0FCA-477D-B1B9-775EA9868B5C}</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/DE_PEX</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[0].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[0].itemID" Type="Str">{E4153AFC-D16F-4C70-A6D3-74A134B82790}</Property>
				<Property Name="Source[0].newName" Type="Str">DE_PEX_</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].newName" Type="Str">DE_PEX.xctl</Property>
				<Property Name="Source[1].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[1].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[1].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX Uninit.vi</Property>
				<Property Name="Source[10].newName" Type="Str">DE_PEX_Uninit.vi</Property>
				<Property Name="Source[10].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[10].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[10].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[10].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[10].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[10].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[10].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities</Property>
				<Property Name="Source[2].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[2].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[2].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/SubVIs</Property>
				<Property Name="Source[3].newName" Type="Str">DE_PEX_</Property>
				<Property Name="Source[3].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[3].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[3].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/TypeDefs</Property>
				<Property Name="Source[4].newName" Type="Str">DE_PEX_</Property>
				<Property Name="Source[4].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[4].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[4].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX Convert State For Save.vi</Property>
				<Property Name="Source[5].newName" Type="Str">DE_PEX_Convert State For Save.vi</Property>
				<Property Name="Source[5].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[5].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[5].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[5].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[5].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[5].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[5].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX Data.ctl</Property>
				<Property Name="Source[6].newName" Type="Str">DE_PEX_Data.ctl</Property>
				<Property Name="Source[6].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[6].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[6].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[6].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[6].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[6].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[6].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX Facade.vi</Property>
				<Property Name="Source[7].newName" Type="Str">DE_PEX_Facade.vi</Property>
				<Property Name="Source[7].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[7].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[7].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[7].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[7].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[7].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[7].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX Init.vi</Property>
				<Property Name="Source[8].newName" Type="Str">DE_PEX_Init.vi</Property>
				<Property Name="Source[8].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[8].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[8].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[8].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[8].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[8].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[8].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities/PX State.ctl</Property>
				<Property Name="Source[9].newName" Type="Str">DE_PEX_State.ctl</Property>
				<Property Name="Source[9].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[9].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[9].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[9].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[9].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[9].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[9].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">11</Property>
			</Item>
			<Item Name="Project Explorer XControl PPL" Type="Packed Library">
				<Property Name="Bld_buildCacheID" Type="Str">{9FE5E500-6572-429E-9D0B-A7CDB8137129}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Project Explorer XControl PPL</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Packed Project Libraries/Project Explorer XControl PPL</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{00DBF573-3B9B-4149-94C5-A1D0D548DD36}</Property>
				<Property Name="Destination[0].destName" Type="Str">Project Explorer XControl.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../Packed Project Libraries/Project Explorer XControl PPL/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Packed Project Libraries/Project Explorer XControl PPL</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{5B7BF79F-D7F7-48C3-A6AF-6910CD574DD4}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_autoIncrement" Type="Bool">true</Property>
				<Property Name="TgtF_companyName" Type="Str">Q Software Innovations, LLC</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Project Explorer XControl PPL</Property>
				<Property Name="TgtF_fileVersion.build" Type="Int">5</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">Project Explorer XControl PPL</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 Q Software Innovations, LLC</Property>
				<Property Name="TgtF_productName" Type="Str">Project Explorer XControl PPL</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7528692A-0B3E-44A6-8BFA-2B2664B97CAA}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Project Explorer XControl.lvlibp</Property>
			</Item>
			<Item Name="Project Explorer XControl Source Backup" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">Project Explorer XControl Source Backup</Property>
				<Property Name="Comments" Type="Str"></Property>
				<Property Name="DestinationID[0]" Type="Str">{37789A8F-8BBA-4704-A687-4F80DC22387E}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">1</Property>
				<Property Name="IncludedItems[0]" Type="Ref">/My Computer</Property>
				<Property Name="IncludeProject" Type="Bool">true</Property>
				<Property Name="Path[0]" Type="Path">../../Code Backup/Project Explorer XControl 1.0.1.0.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasevirtual</Property>
			</Item>
		</Item>
	</Item>
</Project>
