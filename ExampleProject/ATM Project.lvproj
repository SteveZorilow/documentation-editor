﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Property Name="NI.Project.Description" Type="Str">Test Project for the &lt;b&gt;Documentation Editor. &lt;/b&gt;&lt;b&gt; &lt;/b&gt; &lt;b&gt; &lt;/b&gt;©2012, Q Software Innovations, LLC.

Written in National Instruments LabVIEW 2012™.</Property>
	<Property Name="varPersistentID:{84C00A27-D295-4B23-96D8-F63CF7A88665}" Type="Ref">/My Computer/Untitled Library 2.lvlib/Variable1</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ATM Controller" Type="Folder" URL="../ATM Controller">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Other File Types" Type="Folder" URL="../Other File Types">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Simulated Database" Type="Folder" URL="../Simulated Database">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Virtual Folder" Type="Folder">
			<Item Name="Untitled 6.vi" Type="VI" URL="../Untitled 6.vi"/>
		</Item>
		<Item Name="XControlVIs" Type="Folder" URL="../XControlVIs">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="ATM Main.vi" Type="VI" URL="../ATM Main.vi"/>
		<Item Name="Class 1.lvclass" Type="LVClass" URL="../Class 1.lvclass"/>
		<Item Name="Global 1.vi" Type="VI" URL="../Global 1.vi"/>
		<Item Name="Global 2.vi" Type="VI" URL="../Global 2.vi"/>
		<Item Name="Menu.rtm" Type="Document" URL="../Menu.rtm"/>
		<Item Name="midi_keyboard.llb" Type="Document" URL="../midi_keyboard.llb"/>
		<Item Name="New Text Document.txt" Type="Document" URL="../New Text Document.txt"/>
		<Item Name="Untitled 7.vi" Type="VI" URL="../Untitled 7.vi"/>
		<Item Name="Untitled Library 1.lvlib" Type="Library" URL="../Untitled Library 1.lvlib"/>
		<Item Name="Untitled Library 2.lvlib" Type="Library" URL="../Untitled Library 2.lvlib"/>
		<Item Name="Untitled.lvtest" Type="TestItem" URL="../Untitled.lvtest">
			<Property Name="utf.vector.test.bind" Type="Str">8575C44D-789E-4068-5369-A41935462AEB</Property>
		</Item>
		<Item Name="Untitled.lvvect" Type="TestVectorItem" URL="../Untitled.lvvect"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
